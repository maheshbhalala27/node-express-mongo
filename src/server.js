const express = require("express");
const dotenv = require("dotenv");
const router = require("./routes/register");
const connectDB = require("./db/connectdb");

const app = express();
dotenv.config()

// env url
const port = process.env.PORT || 3000
const DATABASE_URL = process.env.DATABASE_URL 

app.use(express.json());
app.use(router);

// CORS Policy
app.use(router);

// Database Connection
connectDB(DATABASE_URL)

const start = async () => {
  try {
    app.listen(port, () => console.log(`Server started on port ${port}`));
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};
start();
