const mongoose = require("mongoose");

const RegisterSchema = mongoose.Schema({
    firstName: {
      type: String,
      requied: true,
    },
    lastName: {
      type: String,
      requied: true,
    },
    email: {
      type: String,
      unique: true,
      requied: true,
      trim:true
    },
    password: {
      type: String,
      requied: true,
      trim:true
    },
    date: {
      type: Date,
      default: Date.now,
    },
  });
  
  const RegisterModel = new mongoose.model("Register", RegisterSchema);
  module.exports = RegisterModel