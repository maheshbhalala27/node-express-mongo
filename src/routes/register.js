const express = require("express");
const RegisterModel = require("../models/register");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = new express.Router();

function verifyToken(req, res, next) {
  const beareHeader = req.headers["authorization"];
  if (typeof beareHeader !== "undefined") {
    const bearer = beareHeader.split(" ");
    const token = bearer[1];
    req.token = token;
    next();
  } else {
    res.status(400).send({
      massage: "Token is not vaild",
    });
  }
}

// login
router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (email && password) {
    const user = await RegisterModel.findOne({ email: email });
    if (user !== null) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (user.email === email && isMatch) {
         // Generator Token
        jwt.sign(
          req.body,
          process.env.JWT_SECRET_KEY,
          { expiresIn: "5000s" },
          (err, token) => {
            res.json({
              token,
            });
          }
        );
      } else {
        res.status(400).send({
          massage: "Email or Password not vaild",
        });
      }
    }
  } else {
    res.status(400).send({
      massage: "All filed are requird",
    });
  }
});

// profile
router.post("/profile", verifyToken, async (req, res) => {
   // Generator Verify Token
  jwt.verify(req.token, process.env.JWT_SECRET_KEY, (err, authData) => {
    if (err) {
      res.status(401).send({
        massage: "Invalid token",
      });
    } else {
      res.status(200).send({
        massage: "Profile accessed",
        authData,
      });
    }
  });
});

// new register
router.post("/register", async (req, res) => {
  try {
    const { firstName, lastName, email, password } = req.body;
    const user = await RegisterModel.findOne({ email: email });
    if (!user) {
      if (firstName && lastName && email && password) {
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(password, salt);
        const addNewUser = new RegisterModel({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: hashPassword,
        });
        const insertUser = await addNewUser.save();
        const user = await RegisterModel.findOne({ email: insertUser.email });
        if (user !== null) {
          // Generator Token
          jwt.sign(
            req.body,
            process.env.JWT_SECRET_KEY,
            { expiresIn: "5000s" },
            (err, token) => {
              res.json({
                data:insertUser,
                token,
                massage: "User register successfully",
              });
            }
          );
        } else {
          res.status(401).send({
            massage: "Please Enter Vaild User ??",
          });
        }
      } else {
        res.status(401).send({
          massage: "All fileds are required",
        });
      }
    } else {
      res.status(409).send({
        massage: "User already exists.",
      });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

// get all users
router.get("/user", async (req, res) => {
  try {
    const allUser = await RegisterModel.find({});
    return res.status(200).send(allUser);
  } catch (error) {
    return res.status(401).send(error);
  }
});

// get by id user
router.get("/user/:id", async (req, res) => {
  try {
    const _id = req.params.id;
    const getByIdUser = await RegisterModel.findById({ _id: _id }).select(
      "-password"
    );
    if (getByIdUser) {
      res.status(200).send(getByIdUser);
    } else {
      res.status(404).send({ massage: "Please id is not vaild" });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

// patch user
router.patch("/user/:id", async (req, res) => {
  try {
    const _id = req.params.id;
    const getByIdUser = await RegisterModel.findById({ _id: _id });
    if (getByIdUser) {
      const updateByIdUser = await RegisterModel.findByIdAndUpdate(
        _id,
        req.body,
        {
          new: true,
        }
      ).select("-password");
      res.status(201).send(updateByIdUser);
    } else {
      res.status(404).send({ massage: "Please id is not vaild" });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

// delete user
router.delete("/user/:id", async (req, res) => {
  try {
    const _id = req.params.id;
    const getByIdUser = await RegisterModel.findById({ _id: _id });
    if (getByIdUser) {
      const deleteByIdUser = await RegisterModel.findByIdAndDelete(_id);
      res.status(200).send(deleteByIdUser);
    } else {
      res.status(404).send({ massage: "Please id is not vaild" });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;
