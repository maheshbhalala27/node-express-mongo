const mongoose = require("mongoose");

const connectDB = async (DATABASE_URL) => {
  try {
    await mongoose
      .connect(DATABASE_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useCreateIndex: true,
        // serverSelectionTimeoutMS: 5000,
        // useFindAndModify: true
      })
      .then(() => console.log("MongoDB Connection Successfully..."))
      .catch((err) => console.log("No Connection MongoDb...", err));
  } catch (error) {
    console.log("No Connection MongoDb...", error);
  }
};
module.exports = connectDB;
